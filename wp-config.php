<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'topUxWork');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '123456');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Tx<+#v3.epJ|zHNlxRrJc$R|auCQ518)+GqcpQuL%D @Qob7LM@9C`?|yL,*KyF5');
define('SECURE_AUTH_KEY',  '5/!;pS^z%=p4Hn]QZq+hL.joa?>_|7(j-Tfc4Q(LeG05wQ-BC+7>L 0[+z[+(Fr#');
define('LOGGED_IN_KEY',    'mCW!H5.b%BH BA$8>g?YHe{SIe;%[=(md:K0H bF%>XDl.Ap:5W3V,I|Wy&&J0;i');
define('NONCE_KEY',        '0C=Sk#MiHS9)|x|Wl,SD~YlG$XjIwA+<OVW A+_-UF1!MU$wP$mK3k:gsT<|+~ig');
define('AUTH_SALT',        '4syBJF:~sAL?7ex/q]x7e1}>ZMwt$?8?(F!%dH<;2$65LF>Sw+P_oGL_!|iAfoo3');
define('SECURE_AUTH_SALT', '*.>3uLbpczR}Cp5cekhwm%MUnkG9~WetpM)TvZil)m<cQ(VtM`,rh#O2cY[qA)S|');
define('LOGGED_IN_SALT',   'M%?:oOx+Q7D^]S{b}spZB[CD%#zT0FwYSbMSA?hG-bxB:4MxYp(0q,4SCT[ov*YI');
define('NONCE_SALT',       'Ia>S_/,HPO%5VC.$6%C`8$@sn>>=Jo/.5z-we{6-dR,#e3mSL6#04D63k|lhEX-!');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
