<?php
/**
 * Template Name: sngle collection
 **/
?>

<?php
	get_header();
?>

    <body>
		<?php
			$post = get_post();
			$postId = $post->ID;
			$firstName = esc_html( get_post_meta( $postId, 'firstName', true ));
			$lastName = esc_html( get_post_meta( $postId, 'lastName', true ));
			$fullName = ucfirst($firstName)." ".ucfirst($lastName);
			$avatar = get_post_meta( $postId, "avatar", true);
			$url = esc_html(get_post_meta($postId, 'url', true));
			$headers = get_headers($url);
			$notAllowIframe = in_array("X-Frame-Options: SAMEORIGIN", $headers);
		?>

    	<div class="iframe-container">
			<?php if($notAllowIframe):?>
				<div class="page-not-available">
					<div class="cry-face"></div>
					<div class="big-font">Ahhh</div>
					<div class="waring-description">
						This wesite does not allow its page to display in iframe.
					</div>
					<div class="link">
						<a href="<?php echo $url?>" target="_blank">Clikc here to open it in new tab.</a>
					</div>
				</div>			
			<?php else:?>
				<iframe src="<?php echo $url?>" style="width:100%; height:100%; border: none;" target='_top'></iframe>
			<?php endif;?>
    	</div>

		<div class="ui container">

			<div class="ui top fixed  menu" style="height:54px">
				<div class="left menu">
					
					<?php
						$prev_post = get_next_post();
						if (!empty($prev_post)) {
							$link = get_permalink( $prev_post->ID );
							$preCollectionTitle = esc_html(get_post_meta($prev_post->ID,"collctionTitle", true));
							$title = empty($preCollectionTitle) ? $prev_post->post_title : $preCollectionTitle;
						
					?>
						<a href="<?php echo $link;?>" id="previous" class="item collection-link previous" title="<?php echo $title?>">
							<i class="caret left icon"></i>
					    	<span>Previous</span>
						</a>
					<?php
   					 }
					?>
				</div>
				<div class="ui secondary menu" style="border: none;">
					<div class="item small-avatar">
				    	<img class="ui avatar image" src="<?php echo $avatar["url"]?>">
					</div>
					<h4 class="header item" >
					  <a href="<?php echo $url;?>">	
						<?php echo $fullName;?>
					  </a>
					</h4>
				</div>
				<div class="compact stackable right menu">
					<?php
						$next_post = get_previous_post();
						if (!empty($next_post)) {
							$link = get_permalink( $next_post->ID );
							$nxtCollectionTitle = esc_html(get_post_meta($next_post->ID,"collctionTitle", true));
							$title = empty($nxtCollectionTitle) ? $next_post->post_title : $nxtCollectionTitle;
					?>

						  <a href="<?php echo $link?>" id="next" class="item collection-link next" title="<?php echo $title?>">
						    <i class="caret right icon"></i>
						    <span>Next</span>
						  </a>
				  <?php
				    }
				  ?>
				  <a class="item home-link" href="<?php echo site_url()?>">
				    <i class="grid layout icon"></i>
				    <span>Home</span>
				  </a>
				</div>
			</div>
		</div>

		<script type="text/javascript">
			$(document).ready(function() {
				$('.special.cards .image').dimmer({
				  on: 'hover'
				});

				$('#previous').popup({
				    on: 'hover',
				    offset: 20,
				    delay: {
				      show: 500,
				      hide: 800
				    }
				});

				$('#next').popup({
				    on: 'hover',
				    delay: {
				      show: 500,
				      hide: 800
				    }
				});

			});
		</script>
    </body>
</html>
