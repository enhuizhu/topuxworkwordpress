<?php
/*
Plugin Name: Collections
Plugin URI: http://www.olmarket.co.uk
Description: plugin to manage Collections
Version: 1.0
Author URI: http://www.olmarket.co.uk
*/

// register custom post type to work with
function wpmudev_create_post_type() {
	// set up labels
	$labels = array(
 		'name' => 'Collections',
    	'singular_name' => 'Collection',
    	'add_new' => 'Add New Collection',
    	'add_new_item' => 'Add New Collection',
    	'edit_item' => 'Edit Collection',
    	'new_item' => 'New Collection',
    	'all_items' => 'All Collections',
    	'view_item' => 'View Collection',
    	'search_items' => 'Search Collections',
    	'not_found' =>  'No Collection Found',
    	'not_found_in_trash' => 'No Collection found in Trash', 
    	'parent_item_colon' => '',
    	'menu_name' => 'Collections',
    );
    //register post type
	register_post_type( 'collection', array(
		'labels' => $labels,
		'has_archive' => true,
 		'public' => true,
		'supports' => array( 'title', 'editor', 'thumbnail'),
		// 'taxonomies' => array( 'post_tag', 'category' ),	
		'exclude_from_search' => false,
		'capability_type' => 'post',
		'rewrite' => array( 'slug' => 'Collections' ),
		)
	);
}

function disply_collection_meta_box($collection){
        wp_nonce_field(plugin_basename(__FILE__), 'avatar_nonce');
        
        $collctionTitle = get_post_meta( $collection->ID, 'collctionTitle', true );    
        // die(var_dump($collctionTitle));
        $firstName = esc_html( get_post_meta( $collection->ID, 'firstName', true ) );    
        $lastName = esc_html( get_post_meta( $collection->ID, 'lastName', true ) );    
        $jobTitle = esc_html( get_post_meta ($collection->ID, 'jobTitle', true) );
        $url = esc_html( get_post_meta( $collection->ID, 'url', true ) );
        $displayInIframe = esc_html( get_post_meta ($collection->ID, 'displayInIframe', true) );
        $description = esc_html( get_post_meta( $collection->ID, 'description', true ) );
        $avatar = get_post_meta( $collection->ID, "avatar", true);    
        $avatarImage = empty($avatar) ? "" : "<img src='".$avatar["url"]."' style='max-width:100px; max-height:100px;'>";

        echo '
            <table width="100%">
               <tr>
                    <td>Collection Title:</td>
                    <td><Input type="text" name="collctionTitle" value="'.$collctionTitle.'"></td>
               </tr>

               <tr>
                    <td>First Name:</td>
                    <td><Input type="text" name="firstName" value="'.$firstName.'"></td>
               </tr>
               <tr>
                    <td>Last Name:</td>
                    <td><Input type="text" name="lastName" value="'.$lastName.'"></td>
               </tr>

               <tr>
                    <td>job title:</td>
                    <td><Input type="text" name="jobTitle" value="'.$jobTitle.'"></td>
               </tr>

               <tr>
                    <td>URL:</td>
                    <td><Input type="text" name="url" value="'.$url.'"></td>
               </tr>
               
               <tr>
                    <td>Display In Iframe:</td>
                    <td><Input type="text" name="displayInIframe" value="'.$displayInIframe.'"></td>
               </tr>

               <tr>
                    <td>Short Description:</td>
                    <td>
                        <textarea name="description">'.$description.'</textarea>
                    </td>
               </tr>
               <tr>
                    <td>Avatar</td>
                    <td><input type="file" name="avatar">'.$avatarImage.'</td>
               </tr>
            </table>
        ';
}

/**
* function to check if url already exist
**/
function isUrlExist($collection_id, $url){
    global $wpdb;
    $sql = "select count(*) as n from wp_postmeta where meta_key='url'";
    $sql.= " and meta_value='$url'";
    $sql.= " and post_id!=$collection_id";
    $result = $wpdb->get_row($sql);
    return $result->n > 0;
}



function add_collection_fields($collection_id, $collection){
        /**
        * should check post type
        **/
        if ($collection->post_type == "collection") {
            if (isset($_POST["collctionTitle"]) && !empty($_POST["collctionTitle"])) {
                update_post_meta($collection_id, "collctionTitle", $_POST["collctionTitle"]);
            }
            
            if (isset($_POST["firstName"]) && !empty($_POST["firstName"])) {
                update_post_meta($collection_id, "firstName", $_POST["firstName"]);
            }

            if (isset($_POST["lastName"]) && !empty($_POST["lastName"])) {
                update_post_meta($collection_id, "lastName", $_POST["lastName"]);
            }

            if (isset($_POST["jobTitle"]) && !empty($_POST["jobTitle"])) {
                update_post_meta($collection_id, "jobTitle", $_POST["jobTitle"]);
            }

            if (isset($_POST["displayInIframe"]) && !empty($_POST["displayInIframe"])) {
                update_post_meta($collection_id, "displayInIframe", $_POST["displayInIframe"]);
            }

            if (isset($_POST["url"]) && !empty($_POST["url"])) {
                if (isUrlExist($collection_id, $_POST["url"])) {
                    wp_die("url already exist");
                }
                update_post_meta($collection_id, "url", $_POST["url"]);
            }


            if (isset($_POST["description"]) && !empty($_POST["description"])) {
                update_post_meta($collection_id, "description", $_POST["description"]);
            }
            /**
            * update avatar 
            **/
            if (!empty($_FILES["avatar"]["name"])) {
                $supported_types = array("image/jpeg","image/png","image/bmp");
                
                if (!in_array($_FILES["avatar"]["type"], $supported_types)) {
                    wp_die("please uplaod jpg or png!");
                }

                /**
                * everthing is fine, should upload the image.
                **/
                $upload = wp_upload_bits($_FILES['avatar']['name'], null, file_get_contents($_FILES['avatar']['tmp_name']));

                if (isset($upload["error"]) && $upload["error"]!=0){
                    wp_die('There was an error uploading your file. The error is: ' . $upload['error']);
                }
                    
                /**
                * no error, save upload
                **/
                // add_post_meta($id, 'avatar', $upload);
                update_post_meta($collection_id, "avatar", $upload);
            }


        }
};

function include_template_function( $template_path ) {
    if ( get_post_type() == 'collection' ) {
        if ( is_single() ) {
            // checks if the file exists in the theme first,
            // otherwise serve the file from the plugin
            if ( $theme_file = locate_template( array ( 'display_single_collection.php' ) ) ) {
                $template_path = $theme_file;
            } else {
                $template_path = plugin_dir_path( __FILE__ ) . '/display_single_collection.php';
            }
        }else{
            if ( $theme_file = locate_template( array ( 'all_collections.php' ) ) ) {
                $template_path = $theme_file;
            } else {
                $template_path = plugin_dir_path( __FILE__ ) . '/all_collections.php';
            }   
        }
    }
    return $template_path;
}


function update_edit_form() {
    echo ' enctype="multipart/form-data"';
} 

function my_admin(){
     add_meta_box('collection_meta_box', 'Collection Details', 'disply_collection_meta_box', 'collection', 'normal', 'high');
}

add_action( 'init', 'wpmudev_create_post_type' );
add_action( 'admin_init', 'my_admin');
add_action( 'save_post', 'add_collection_fields', 10, 2 );
add_action( 'post_edit_form_tag', 'update_edit_form');
add_filter( 'template_include', 'include_template_function', 1 );





