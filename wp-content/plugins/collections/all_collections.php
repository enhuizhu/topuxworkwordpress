<?php
/**
 * Template Name: all collections
 **/
?>

 <?php
    get_header();
 ?> 
    <body>
		<div class="ui container">
			<?php
 				get_menus();
			?>
			<h1 class="ui center aligned header">Top UX Work
				<div class="sub header">Collection of Top UX Portfolios</div>
			</h1>
			<div class="ui hidden divider"></div>
			<div class="ui hidden divider"></div>
			<div class="ui hidden divider"></div>
			<div class="ui hidden divider"></div>
			<div class="ui three doubling stackable special cards link">
				<?php
					$mypost = array( 'post_type' => 'collection', );
				    $collections = new WP_Query( $mypost );
				    // die(var_dump($collections));
				    // wp_die($collections);
				    while ($collections->have_posts()) {
				    	$collections->the_post();
				    	$postId = get_the_id();
				    	$link = get_permalink($postId);
				    	$featureImg = wp_get_attachment_url( get_post_thumbnail_id($postId) );
				    	$firstName = esc_html( get_post_meta( $postId, 'firstName', true ));
						$lastName = esc_html( get_post_meta( $postId, 'lastName', true ));
						$description = esc_html( get_post_meta( $postId, 'description', true));
						$fullName = ucfirst($firstName)." ".ucfirst($lastName);
						$avatar = get_post_meta( $postId, "avatar", true);
						$url = esc_html(get_post_meta($postId, 'url', true));
						$jobTitle = esc_html(get_post_meta($postId, "jobTitle", true));
				?>
				
				<div class="fluid card">
					<a class=" dimmable image" href="<?php echo $link?>">
				      <div class="ui dimmer">
				        <div class="content">
				          <div class="center">
				            <i class="huge zoom icon"></i>
				          </div>
				        </div>
				      </div>
				      <img src="<?php echo $featureImg?>">
				    </a>
					<div class="content">
						<img class="ui avatar image right floated" src="<?php echo $avatar["url"];?>">
						<a class="header" href="#"><?php echo $fullName?></a>
						<div class="meta">
						  <span class="date"><?php echo $jobTitle?></span>
						</div>
						<div class="description">
							<?php echo $description;?>
						</div>
					</div>
					<div class="extra content">
						<a>
						  <i class="heart outline like icon"></i>
					      17 likes
						</a>
					</div>
				</div>
				
				<?php
				    }
				?>

			</div>

			<?php
			  get_footer();
			?>

		</div>



    </body>
</html>
