<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css" integrity="sha384-aUGj/X2zp5rLCbBxumKTCw2Z50WgIr1vs/PFN4praOTvYXWlVyh2UtNUU0KAUhAX" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>

<table class="table table-striped">
  <tr>
  	<th>Post Id</th>
    <th>user Id</th>
    <th>Platform</th>
    <th>Create At</th>
    <th>Delete</th>
  </tr>
  <?php foreach($likeObj->records as $like):?>
  	<tr>
  		<td><?php echo get_the_title( $like->post_id );?></td>
  		<td><?php echo getUserName($like->user_id, $like->user_platform);?></td>
      <td><?php echo $like->user_platform;?></td>
  		<td><?php echo $like->create_at;?></td>
  		<td><a href="<?php echo generatePaginationUrl($adminUrl,$likeObj->currentPage,"&delete=".$like->id)?>" class="btn btn-danger">Delete</a></td>
  	</tr>
  <?php endforeach;?>
</table>


<center>
<nav>
   <ul class="pagination">
   		<?php
   			$isPreviousDisabled = $likeObj->currentPage == 1;
   		?>
   		<li class="<?php echo $isPreviousDisabled ? "disabled" : "" ?>">
   			<a href="<?php echo $isPreviousDisabled ? "javascript:void(0);" : generatePaginationUrl($adminUrl, $likeObj->currentPage - 1) ?>" aria-label="Pevious">
   			    <span aria-hidden="true">&laquo;</span>
   			</a>
   		</li>
   		<?php
   			for($i=1; $i <= $likeObj->totalPages; $i++):
   		?>
   		<li class="<?php echo $i==$likeObj->currentPage ? "active" : ""?>">
   			<a href="<?php echo generatePaginationUrl($adminUrl, $i)?>"><?php echo $i;?></a>
   		</li>
   		<?php
   			endfor;
   		?>
   		
   		<?php
   			$isNextDisabled = $likeObj->currentPage == $likeObj->totalPages;
   		?>

   		<li class="<?php echo $isNextDisabled ? "disabled" : "" ?>">
   			<a href="<?php echo $isNextDisabled ? "javascript:void(0);" : generatePaginationUrl($adminUrl, $likeObj->currentPage + 1) ?>" aria-label="Pevious">
   			    <span aria-hidden="true">&raquo;</span>
   			</a>
   		</li>
   </ul>
</nav>
</center>