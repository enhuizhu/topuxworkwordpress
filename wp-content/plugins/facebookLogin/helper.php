<?php

if (!function_exists("generatePaginationUrl")) {
	function generatePaginationUrl($basicUrl, $requestPage, $extraStr = null){
		$url = $basicUrl."&currentPage=".$requestPage;
		if (!empty($extraStr)) {
			$url .= $extraStr;
		}
		return $url;
	}
}

if (!function_exists("getUserName")) {
	function getUserName($userId, $platform){
		 global $dbService;
		 switch ($platform) {
		 	case 'wordpress':
		 		$userInfo = get_userdata($userId);
		 		return $userInfo->user_login;
		 	case "facebook":
		 	    return $dbService->getFacebookUser($userId)->username;
		 	default:
		 		return false;
		 }
	}
}

/**
* function to generate ajax url to js file
**/

if (!function_exists("generateAjaxUrlToJs")) {
	function generateAjaxUrlToJs(){
		/**
		* should check if file already exist
		**/
		if (file_exists("js/ajaxUrl.js")) {
			return false;
		}

		$jsFile = fopen(__DIR__. "/js/ajaxUrl.js", "w") or die("unable to open file js/ajaxUrl.js;");

		fwrite($jsFile, "var ajaxUrl ='".get_site_url()."/wp-admin/admin-ajax.php'");

		fclose($jsFile);
	}
}


if (!function_exists("addJsToFooter")) {
	function addJsToHeader(){
        // echo '<meta name="viewport" content="width=device-width, initial-scale=1">';
	    echo '<script>
	    	var ajaxUrl = "'.site_url("wp-admin/admin-ajax.php").'";
	    </script>'."\n";
	    echo '<script language="javascript" src="'.plugins_url('js/likeClass.js', __FILE__).'"></script>'."\n";
	    echo '<script language="javascript" src="'.plugins_url('js/login.js', __FILE__).'"></script>'."\n";
	}
}



if (!function_exists("custom_login")) {
	function custom_login($username, $password) {
	    $creds = array('user_login' => $username, 'user_password' => $password, 'remember' => true );
	    $user = wp_signon( $creds, false );
	    wp_set_current_user($user->ID); 
	    wp_set_auth_cookie($user->ID, true, false );    
	    do_action( 'wp_login', $username );

		$result = array();

	    if ( is_wp_error($user) ){
	        $result["success"] = false;
	    	$result["messsage"] = $user->get_error_message();
	    }else{
	    	$result["success"] = true;
	    }

	    return $result;
    }
}


if (!function_exists("getFacebookLoginUrl")) {
	function getFacebookLoginUrl(){
		global $facebookApi;
	    $loginUrl = $facebookApi->getLoginUrl();
	    return $loginUrl;
	}
}


if(!function_exists("isUserLoggedIn")){
	function isUserLoggedIn(){
		global $facebookApi;
		/**
		* shoud check if user login with facebook
		**/
		$result = array();

		$response = $facebookApi->isUserLogin();

		if ($response) {
			$result["isLogin"] = true;
			$result["platform"] = "facebook";
			$result["userId"] = $response->getId();

			return $result;
		}

		if (is_user_logged_in()) {
			$result["isLogin"] = true;
			$result["platform"] = "wordpress";
			$result["userId"] = get_current_user_id();
			return $result;
		}

		return false;
	}
}

if (!function_exists("getLogoutUrl")) {
	function getLogoutUrl($platform){
		global $facebookApi;		
		
		switch ($platform) {
			case 'wordpress':
				return wp_logout_url(get_home_url());
			case "facebook" :
				return $facebookApi->getLogoutUrl();
		}

		return false;
	}
}

if (!function_exists("getUserInfo")) {
	function getUserInfo($platform){
		// die("here");
		switch ($platform) {
			case "wordpress":
				return getWordpressUserInfo();
			case "facebook":
				return getFacbookUserInfo();
		}

		throw new Exception("$platform is not supoorted", 1);
	}
}

if (!function_exists("getWordpressUserInfo")) {
	function getWordpressUserInfo(){
		$userInfo = wp_get_current_user();
		return array(
				  "username" => $userInfo->user_login,
				  "id" => $userInfo->ID,
				  "email" => $userInfo->user_email,
				  "platform" => "wordpress",
			   );
	}
}

if (!function_exists("getFacbookUserInfo")) {
	function getFacbookUserInfo(){
		// die("here! facebook");
		global $facebookApi, $dbService;
		$userInfo = $facebookApi->getUserInfo();
		$infoArr = array(
				"id"=>$userInfo->getId(),
				"email"=>$userInfo->getEmail(),
				"username"=>$userInfo->getName(),
				"platform" => "facebook",
			);
		/**
		* should check if user already in the table, if not in the table 
		* should insert this user
		**/
		$isUserExist = $dbService->isUserExist($infoArr["id"]);
		
		// die(var_dump($isUserExist));
		
		if (!$dbService->isUserExist($infoArr["id"])) {
		    $result = $dbService->createUser($infoArr);
		    
		    if (!$result) {
		    	die("there was a mysql error when try to crate new user!");
		    }
		}

		return $infoArr;
	}
}
