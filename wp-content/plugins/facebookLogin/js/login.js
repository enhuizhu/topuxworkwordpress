/**
* js class control the login form
**/
var login = {
	 init : function(){
			var that = this;

			this.loginModal = jQuery("#loginForm");
			this.loginForm = this.loginModal.find(".ui.form");

			this.loginModal.modal({
				detachable: true, 
				closable: true, 
				onApprove : function() {
				  that.loginForm.submit();
				  return false;
				},
			});

			this.initFormValidation();
			this.bootstrapEventListeners();
	 },

	 unbindLikeEvent : function(){
	 	jQuery(".like").unbind("click");
	 },

	 bootstrapEventListeners : function(){
	 	var that = this;
	 	
	 	jQuery(".like").click(function(){
	 		 var domObj = jQuery(this);

	 		 /**
	 		 * should check if user already login
	 		 **/
	 		 if (!wpInfo.isLogin) {
	 		 	 that.loginModal.modal("show");
	 		 }else{
		 		 /**
		 		 * get the post id
		 		 **/
		 		 var postId = domObj.attr("data-postId"),
		 		 	 data = "postId="+postId+"&action=addLike";
		 		 
		 		 jQuery.ajax({
		 		 	url: ajaxUrl,
		 		 	data : data,
		 		 	type : "post",
		 		 	beforeSend : function(xhr){
		 		 		jQuery(".loader-wrapper").show();
		 		 	},
		 		 	success : function(data){
		 		 		var result = JSON.parse(data);
		 		 		
		 		 		jQuery(".loader-wrapper").hide();
		 		 		
		 		 		if (!result.success) {
		 		 			// alert(result.msg);
		 		 			return false;
		 		 		};

		 		 		domObj.find(".like-number").html(result.count);
		 		 		
		 		 		if (result.action == "remove") {
		 		 			domObj.removeClass("disable").find(".heart").addClass("outline");
		 		 	    }else{
		 		 			domObj.addClass("disable").find(".heart").removeClass("outline");
		 		 			/**
		 		 			* make liked-layer active
		 		 			**/
		 		 			var likedLayer = domObj.next();
		 		 			
		 		 			likedLayer.addClass("active");
		 		 			
		 		 			setTimeout(function(){
		 		 				likedLayer.removeClass("active");
		 		 			},300)
		 		 	    }

		 		 	}
		 		 })

		 		 // console.info("the value of postId is:", postId);
	 		 }
	 	});

	 	jQuery("#loginLink").click(function(){
	 		  /**
	 		  * user clicked login link should display login modal
	 		  **/
	 		  that.loginModal.modal("show");
	 	});


	 	jQuery(".fackebookLogin").bind("click", this.loginWithFacebook.bind(this));
	 },

	 loginWithFacebook : function(){
	 	console.info("login with facebook clicked");
	 },


	 initFormValidation : function(){
		
		var formValidationRules ={
				username: {
				  identifier : 'username',
				  rules: [
					{
					  type   : 'empty',
					  prompt : 'Please enter username'
					}
				  ]
				},
				password: {
				  identifier : 'password',
				  rules: [
					{
					  type   : 'empty',
					  prompt : 'Please enter password'
					}
				  ]
				},
			},
	        
	        formSettings ={
				onSuccess : function() 
				{
				  
				  console.info("validation success");
				  // suggestWorkModal.modal('hide');
				},
			};

		this.loginForm.form(formValidationRules, formSettings);

	 },

}


jQuery(document).ready(function(){
	/**
	* init the login form
	**/
	login.init();

})
