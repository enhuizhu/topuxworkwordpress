<?php
/**
* all the functions that related to ajax request 
* will come here
**/
add_action("wp_ajax_addLike", "addLike");
add_action("wp_ajax_nopriv_addLike", "addLike");
if(!function_exists("addLike")){
	   function addLike(){
	        global $dbService;
	        /**
	        * should check if user already login
	        **/
	        $result = isUserLoggedIn();

	        if (!$result) {
	        	echo json_encode(array("success"=>false, "msg"=>"user not has not login yet!"));
	        }

	        echo json_encode($dbService->addLike($_POST, $result["platform"], $result["userId"]));

	        die();
	   }
}
