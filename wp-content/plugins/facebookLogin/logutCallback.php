<?php
require_once __DIR__ . '/vendor/autoload.php';
require_once "fbApi.php";

session_start();
    
$facebookApi = new fbApi();

$facebookApi->destorySession();

$redirectUrl = $_GET["site_url"];

header("Location: $redirectUrl");

die();
