<?php
/*
Plugin Name: facebook login
Plugin URI: http://www.olmarket.co.uk
Description: plugin to integrate with facebook api
Version: 1.0
Author: Enhui Zhu
Author URI: http://www.olmarket.co.uk
*/
session_start();

require_once __DIR__ . '/vendor/autoload.php';
require_once "fbApi.php";
require_once "dbService.php";
    
$facebookApi = new fbApi();
$dbService  = new dbService();

require_once "helper.php";
require_once "likeApi.php";
/**
* generate ajax url
**/
add_action('wp_head', 'addJsToHeader');

$userInfo = null;


function like_admin_action(){
       add_menu_page("Likes Of Collection", "Likes Of Collection", 1, "Likes Of Collection", "like_admin");
}
 
add_action('admin_menu', 'like_admin_action');


function like_admin(){
    global $dbService;
    /**
    * should check if url conations delete
    **/
    if (isset($_GET["delete"])) {
    	$dbService->deleteLike($_GET["delete"]);
    }

    $recoredsPerPage = 10;
    $currentPage = isset($_GET["currentPage"]) ? $_GET["currentPage"] : 1;
    $adminUrl = admin_url( "admin.php?page=".$_GET["page"]);
    /**
    * get all the like post 
    **/
    $likeObj = $dbService->getLikes($recoredsPerPage, $currentPage);
    
    include "views/index.php";
}