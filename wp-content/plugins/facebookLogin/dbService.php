<?php
class dbService {
	function __construct(){
		global $wpdb;
		$this->db = $wpdb;
		$this->createTables();
	}	

	public function createTables(){
		$this->createFacebookUserTable();
		$this->createLikeTable();
	}

	public function createFacebookUserTable(){
	   if($this->isTableExsit("facebookUsers")) return true;
        
        $sql="CREATE TABLE facebookUsers(
             id bigint(20) AUTO_INCREMENT,
             facebookid bigint(20),
             username varchar(100),
             email varchar(100),
             PRIMARY KEY (id)
          );";
        
        return $this->db->query($sql);
	}

	public function createLikeTable(){
	    if($this->isTableExsit("likePosts")) return true;
        
        $sql="CREATE TABLE likePosts(
             id bigint(20) AUTO_INCREMENT,
             post_id bigint(20),
             user_id bigint(20),
             user_platform varchar(20),
             PRIMARY KEY (id)
          );";
        
        return $this->db->query($sql);
	}


	public function isTableExsit($table){
    	$sql="show tables like '$table'";
    	$result = $this->db->get_results($sql);
    	return empty($result)?false:true;
    }

    public function isUserExist($facebookid){
    	$sql="select count(*) as n from facebookUsers where facebookid=".$this->dbQuotes($facebookid);
    	$result = $this->db->get_row($sql);
    	return $result->n > 0;
    }

    public function createUser($userInfo){
    	$sql="insert into facebookUsers (facebookid, username, email) values (";
    	$sql.= $this->dbQuotes($userInfo["id"]).",";
    	$sql.= $this->dbQuotes($userInfo["username"]).",";
    	$sql.= $this->dbQuotes($userInfo["email"]).")";
		
		return $this->db->query($sql);
    }

    public function dbQuotes($str){
    	return "'".esc_sql($str)."'";
    }

    /**
    * check if user already like this post
    **/
    public function isUserAlreadyLikePost($postId, $userId, $platform){
    	 $sql = "select count(*) as n from likePosts where post_id=".$this->dbQuotes($postId);
    	 $sql.= " and user_id=".$this->dbQuotes($userId);
    	 $sql.= " and user_platform=".$this->dbQuotes($platform);

    	 $result = $this->db->get_results($sql);
    	 return $result[0]->n > 0 ? true : false;
    }

    public function getTotalLike($postId = null){
        $sql = "select count(*) as n from likePosts";

        if (!empty($postId)) {
            $sql.=" where post_id=".$this->dbQuotes($postId);
        }

        $result = $this->db->get_results($sql);

    	return $result[0]->n;
    }

    public function removeLike($postId, $userId,$platform){
        $sql = "delete from likePosts where post_id=".$postId;
        $sql .= " and user_id=".$userId;
        $sql .= " and user_platform=".$this->dbQuotes($platform);

        // echo "$sql";

        return $this->db->query($sql);
    }

    public function addLike($var,$platform, $userId){
    	if ($this->isUserAlreadyLikePost($var["postId"], $userId, $platform)) {
    		if ($this->removeLike($var["postId"], $userId, $platform)) {
                return array(
                        "success" => true,
                        "action" => "remove",
                        "count" => $this->getTotalLike($var["postId"])
                    );

            }
            
            return array(
    				 "success" => false,
    				 "msg" => "mysql error!"
    				);
    	}
    	
    	$sql = "insert into likePosts (post_id, user_id, user_platform, create_at) values (";
    	$sql .= $this->dbQuotes($var["postId"]).",";
    	$sql .= $this->dbQuotes($userId).",";
        $sql .= $this->dbQuotes($platform).",";
    	$sql .= $this->dbQuotes(date("Y-m-d H:i:s")).")";
		
		if ($this->db->query($sql)) {
			return array(
					 "success" => true,
                     "action" => "add",
					 "count" => $this->getTotalLike($var["postId"])
					);
		}else{
			 return array(
			 		  "success" => false,
			 		  "msg" => "mysql error!"
			 		);
		}
    }

    public function getFacebookUser($userId){
        $sql = "select * from facebookUsers where facebookid=$userId";
        $row = $this->db->get_row($sql);
        return $row;
    }

    public function getLikes($recoredsPerPage,$currentPage){
        $totalLikes = $this->getTotalLike();
        /**
        * get homw many pages it has
        **/
        $totalPages = ceil($totalLikes / $recoredsPerPage);
        /**
        * should check if $currentPage is bigger then $totalPage
        **/
        if ($currentPage > $totalPages) {
            $currentPage = $totalPages;
        }
        
        $startIndex = $recoredsPerPage * ($currentPage - 1);

        $sql = "select * from likePosts order by create_at desc limit ".$startIndex.",".$recoredsPerPage;
        $results = $this->db->get_results($sql);
        
        $obj = new StdClass;
        $obj->totalPages = $totalPages;
        $obj->currentPage = $currentPage;
        $obj->records = $results;

        return $obj;
    }

    public function deleteLike($likeId){
        $sql = "delete from likePosts where id = $likeId";
        return $this->db->query($sql);
    }


}