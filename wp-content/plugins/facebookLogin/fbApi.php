<?php

require_once "config.php";

class fbApi{
		
	protected $fb = "";

	protected $defautAccessToken = "topuxwork";

	protected $appId = APPID;

	protected $appSecret = APPSECRET;
	
	public function __construct(){

		$this->fb = new Facebook\Facebook([
		  'app_id' => $this->appId,
		  'app_secret' => $this->appSecret,
		  'default_access_token' => $this->defautAccessToken,
		  'default_graph_version' => 'v2.2',
		]);

	}

	public function getToken(){
		if (isset($_SESSION["facebook_access_token"]) && !empty($_SESSION["facebook_access_token"])) {
			return $_SESSION["facebook_access_token"];
		}

		return false;
	}

	public function setToken(){
		date_default_timezone_set("Europe/London");

		$helper = $this->fb->getRedirectLoginHelper();

		try {
		  $accessToken = $helper->getAccessToken();
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
		  // When Graph returns an error
		  echo 'Graph returned an error: ' . $e->getMessage();
		  exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
		  // When validation fails or other local issues
		  echo 'Facebook SDK returned an error: ' . $e->getMessage();
		  exit;
		}

		if (isset($accessToken)) {
		  // Logged in!
		  $_SESSION['facebook_access_token'] = (string) $accessToken;

		  // Now you can redirect to another page and use the
		  // access token from $_SESSION['facebook_access_token']
		}
		// OAuth 2.0 client handler
		$oAuth2Client = $this->fb->getOAuth2Client();

		// Exchanges a short-lived access token for a long-lived one
		$longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);

		$_SESSION["facebook_access_token_long_time"] = $longLivedAccessToken;
	}

	public function destorySession(){
		unset($_SESSION["facebook_access_token"]);
		unset($_SESSION["facebook_access_token_long_time"]);
	}


	public function getLoginUrl(){
	    $loginCallBackUrl = plugin_dir_url(__FILE__)."loginCallback.php?site_url=".site_url();
		$helper = $this->fb->getRedirectLoginHelper();
		$loginUrl = $helper->getLoginUrl($loginCallBackUrl);
		
		return $loginUrl;
	}


	public function isUserLogin(){
		$token = $this->getToken();

		if (!$token) {
			return false;
		}

		try {

		  $this->fb->setDefaultAccessToken($token);
		  $response = $this->fb->get('/me', $token)->getGraphUser();
		  // $userNode = $response->getGraphUser();
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
		  // return false;
		  // When Graph returns an error
		  echo 'Graph returned an error: ' . $e->getMessage();
		  exit;
		  return false;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
		  // return false;
		  // When validation fails or other local issues
		  echo 'Facebook SDK returned an error: ' . $e->getMessage();
		  exit;
		  return false;
		}

		return $response;
	}

	public function getLogoutUrl(){
		$token = $this->getToken();
		
		if (!$token) {
			throw new Exception('User already logout!');
		}
		
	 	$logutCallBackUrl = plugin_dir_url(__FILE__)."logutCallback.php?site_url=".site_url();
		$helper = $this->fb-> getRedirectLoginHelper();
		return $helper->getLogoutUrl($token,$logutCallBackUrl);
	}

	public function getUserInfo(){
		/**
		* should check if user already logn or not
		**/
		try {
		  $url = '/me?fields=name,first_name,last_name,email&access_token='.$this->getToken();
		  $response = $this->fb->get($url);
		  $userNode = $response->getGraphUser();
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
		  // When Graph returns an error
		  echo 'Graph returned an error: ' . $e->getMessage();
		  exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
		  // When validation fails or other local issues
		  echo 'Facebook SDK returned an error: ' . $e->getMessage();
		  exit;
		}

		return $userNode;	
	}

}