<!DOCTYPE html>
<html>
    <head>
        <title><?php echo get_the_title()?></title>
        <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo  get_bloginfo("template_url")."/js/semantic.min.js"; ?>"></script>
        <script type="text/javascript" src="<?php echo  get_bloginfo("template_url")."/js/dimmer.min.js"; ?>"></script>
        <script type="text/javascript" src="<?php echo  get_bloginfo("template_url")."/js/modal.min.js"; ?>"></script>
        <script type="text/javascript" src="<?php echo  get_bloginfo("template_url")."/js/form.min.js"; ?>"></script>
    	<?php wp_head(); ?>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
