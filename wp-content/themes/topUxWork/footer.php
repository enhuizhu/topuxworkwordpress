<div class="ui small modal" id="suggestWork">
	<div class="ui header">Suggest a work
	  	<div class="sub header">Please fill in the form below to submit the great UX portfolio you recommend!</div>
    </div>
    <div class="content">
	    <form class="ui form " action="https://getform.org/f/80301b7f-4192-49f9-9de9-998b576d267b" method="POST">
		    <div class="ui padding basic segment">
			    <div class="required field">
				    <label>Porfolio:</label>
				    <input type="text" name="portfolio" placeholder="Portfolio Website">
			    </div>
			  	<div class="field">
				    <label>Designer:</label>
				    <input type="text" name="name" placeholder="Full Name">
			    </div>
			    <div class="field">
				    <label>(Optional)</label>
				    <textarea rows="2" name="reason" placeholder="Why this portfolio is great?"></textarea>
			    </div>
			    <div class="ui error message"></div>
		    </div>
        </form>
    </div>
    <div class="actions">
	    <div class="ui cancel button">Cancel</div>
	    <div class="ui primary approve button">Submit</div>
    </div>
</div>

<div id="loginForm" class="ui small basic modal">
				<div class="ui middle aligned center aligned grid">
				    <div class="ui column">
				        <div class="ui segment">
				            <div class="ui blue center aligned header">
				                Log in to your account
				            </div>
				            <div class="ui hidden divider"></div>
				            <div class="four ui buttons">
				            	<button class="ui circular facebook icon button" onclick="location.href='<?php echo getFacebookLoginUrl();?>'">
				                	<i class="facebook icon"></i>
				          		</button>
				          		<button class="ui circular twitter icon button">
				              		<i class="twitter icon"></i>
				          		</button>
<!-- 				          		<button class="ui circular linkedin icon button">
				              		<i class="linkedin icon"></i>
				          		</button>
				          		<button class="ui circular google plus icon button">
				              		<i class="google plus icon"></i>
				          		</button>
 -->				        	</div>
				        	<div class="ui horizontal divider">Or</div>

				        	<form class="ui large form" method="post" action="">
				            	<div class="field">
				              		<div class="ui left icon input">
				                		<i class="user icon"></i>
				                		<input type="text" name="username" placeholder="Username">
				              		</div>
				            	</div>
				            	<div class="field">
				              		<div class="ui left icon input">
				                		<i class="lock icon"></i>
				                		<input type="password" name="password" placeholder="Password">
				                		<input type="hidden" name="postLogin" value="1"> 
				              		</div>
				            	</div>
				            	<div class="ui fluid large primary submit button">Login</div>
				          		<div class="ui error message"></div>
							   <!--  <div class="ui success message">
								    <div class="header">Form Completed</div>
								    <p>You're alwl signed up for the newsletter.</p>
								</div> -->
				        	</form>
				        </div>
				        <div class="ui center aligned">
				          Don't have an account? <a href="<?php echo site_url("registration");?>">Sign Up</a>
				        </div>
				    </div>
				</div>
			</div>


<!-- modal for error -->
<div class="ui small modal" id="errorModal">
	<div class="ui header">
		Error
    </div>
    <div class="content">
      <div class="error" id="errorMsg">
      </div>
    </div>
    <div class="actions">
	    <div class="ui cancel button">Cancel</div>
    </div>
</div>
<!-- end error modal -->

<div class="ui hidden divider"></div>
<div class="ui divider"></div>
<div class="content">
	<h5 class="ui center aligned header">
	<div class="sub header">Curated by <a href="">Wei Pan</a>  and <a href="http://www.olmarket.co.uk">Online Marketing Solution</a> with	<i class="heart red icon"></i></div>
	</h5>
</div>
<div class="ui hidden divider"></div>

<div class="loader-wrapper">
<div class="cssload-loader">
	<div class="cssload-inner cssload-one"></div>
	<div class="cssload-inner cssload-two"></div>
	<div class="cssload-inner cssload-three"></div>
</div>
</div>

<script type="text/javascript">
var wpInfo = {
	isLogin : <?php echo (isUserLoggedIn() ? "true" : "false") ?>,
};

/**
* shoulcd check if it has post login detail
**/
<?php
    global $errorMsg;
    if (isset($errorMsg) && !empty($errorMsg)) {
 ?>
  jQuery("#errorModal").modal("show");
  jQuery("#errorMsg").html("<?php echo $errorMsg;?>");
 <?php
    }
 ?>


</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-38576416-4', 'auto');
  ga('send', 'pageview');

</script>


<script type="text/javascript" src="<?php echo  get_bloginfo("template_url")."/js/global.js"; ?>"></script>
<script type="text/javascript" src="<?php echo  get_bloginfo("template_url")."/js/menu.js"; ?>"></script>

