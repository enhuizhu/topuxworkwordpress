<?php
/**
 * Template Name: all collections
 **/
?>
 <?php
    get_header();
 ?> 
    <body>
		<?php 
		  get_userInfo();
		?>

		
		<div class="ui container">
			<?php
 				get_menus();
			?>
			<h1 class="ui center aligned header">Top UX Work
				<div class="sub header">Collection of Top UX Portfolios</div>
			</h1>
			<div class="ui hidden divider"></div>
			<div class="ui hidden divider"></div>
			<div class="ui hidden divider"></div>
			<div class="ui hidden divider"></div>
			<div class="ui three doubling stackable special cards" id="cardContent">

				<?php 
					getCollections(1, false);
				?>

			</div>


			<script type="text/javascript" src="<?php echo  get_bloginfo("template_url")."/js/collectionLoader.js"; ?>"></script>


			<?php
			  get_footer();
			?>

		</div>



    </body>
</html>
