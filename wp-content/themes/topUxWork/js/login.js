/**
* js class control the login form
**/
var login = {

	 init : function(){
			var that = this;

			this.loginModal = jQ("#loginForm");
			this.loginForm = this.loginModal.find(".ui.form");

			this.loginModal.modal({
				detachable: true, 
				closable: true, 
				onApprove : function() {
				  that.loginForm.submit();
				  return false;
				},
			});

			this.initFormValidation();
			this.bootstrapEventListeners();
	 },

	 bootstrapEventListeners : function(){
	 	var that = this;
	 	
	 	jQ(".heart.like").click(function(){
	 		 /**
	 		 * should check if user already login
	 		 **/
	 		 if (!wpInfo.isLogin) {
	 		 	 that.loginModal.modal("show");
	 		 }else{
 		 		 console.info("lick clicked!");
		 		 /**
		 		 * get the post id
		 		 **/
		 		 var postId = jQ(this).attr("data-postId");
		 		 console.info("the value of postId is:", postId);
	 		 }
	 	});

	 	jQ("#loginLink").click(function(){
	 		  /**
	 		  * user clicked login link should display login modal
	 		  **/
	 		  that.loginModal.modal("show");
	 	});


	 	jQ(".fackebookLogin").bind("click", this.loginWithFacebook.bind(this));
	 },

	 loginWithFacebook : function(){
	 	console.info("login with facebook clicked");
	 },


	 initFormValidation : function(){
		
		var formValidationRules ={
				username: {
				  identifier : 'username',
				  rules: [
					{
					  type   : 'empty',
					  prompt : 'Please enter username'
					}
				  ]
				},
				password: {
				  identifier : 'password',
				  rules: [
					{
					  type   : 'empty',
					  prompt : 'Please enter password'
					}
				  ]
				},
			},
	        
	        formSettings ={
				onSuccess : function() 
				{
				  
				  console.info("validation success");
				  // suggestWorkModal.modal('hide');
				},
			};

		this.loginForm.form(formValidationRules, formSettings);

	 },

}
