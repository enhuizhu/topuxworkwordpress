/**
* javascript for menu controle
**/

var menu = {
	bindEvents : function(){
		jQuery(".launch").bind("click", function(event){
			jQuery("#responsiveMenu").toggleClass("show-menu");
			jQuery(".overlay").show();
			event.stopPropagation();
		});

		jQuery(".overlay").bind("click", function(){
			jQuery("#responsiveMenu").removeClass("show-menu");
			jQuery(".overlay").hide();
		})
 }
}

jQuery(document).ready(function(){
	menu.bindEvents();
});