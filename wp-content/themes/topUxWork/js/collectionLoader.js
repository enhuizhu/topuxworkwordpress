var collectionLoader = {
	
	justLoadPage : false,
	
	currentPage : 1,

	loaderAble : true,

	loaderDom : '<div class="ui segment" id="collectionLoader">'+
			  '<div class="ui active inverted dimmer">'+
			    '<div class="ui text loader">Loading more collections</div>'+
			  '</div>'+
			  '<p></p>'+
			'</div>',
	
	bindEvent : function(){
		var that = this;
		
		jQuery('#cardContent')
		  .visibility({
		    once: false,
		    observeChanges: true,
		    onBottomVisible: function() {
	      	  console.info("load more!");
	      	  if (!that.loaderAble) {
		      	  return false;
		      };

		      that.load();
		    }
		  });
	},
	
	load : function(){
			var that = this;

			if (this.justLoadPage) {
			  	  this.justLoadPage = false;
			  	  return ;
			}

			/**
			* should check if #collectionLoader is exist
			**/

			if (jQuery("#collectionLoader").length > 0) {
				return false;
			};
			
			if (this.loaderAble) {
				this.currentPage ++;
			};
			
			var data = "action=loadPage&currentPage="+this.currentPage;

			jQuery.ajax({
				type: "post",
				url : ajaxUrl,
				data : data,
				beforeSend: function(xhr){
					jQuery("#cardContent").append(that.loaderDom);
				},
				success: function(response){
					setTimeout(function(){
						jQuery("#collectionLoader").remove();

						if (jQuery.trim(response)=="false") {
							that.loaderAble = false;
							return false;
						};

						jQuery("#cardContent").append(response);
						
						login.unbindLikeEvent();
						login.bootstrapEventListeners();
						initCards();


						},100);
				}
			});
	}
}


jQuery(document).ready(function(){
	collectionLoader.bindEvent();
});
