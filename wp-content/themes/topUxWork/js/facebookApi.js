/**
* facebook class to interact with facebook api
**/

var facebookApi = {
    
    currentStatus: null,
	
	config :{
		appId : 1672429202997818,
	},
	
	init : function(){
		var that = this;
		
		window.fbAsyncInit = function() {
			  FB.init({
			    appId      : that.config.appId,
			    cookie     : true,  // enable cookies to allow the server to access 
			                        // the session
			    xfbml      : true,  // parse social plugins on this page
			    version    : 'v2.2' // use version 2.2
			  });
		}

		FB.getLoginStatus(function(response) {
		    that.statusChangeCallback(response);
		});
 },

 testAPI : function(){
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
      console.info("response",response);
      console.log('Successful login for: ' + response.name);
    });
 },

 // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  checkLoginState: function() {
    var that = this;
    
    FB.getLoginStatus(function(response) {
      that.statusChangeCallback(response);
    });
  },

 loginToSite: function(){

 },

 statusChangeCallback: function(response){
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    this.currentStatus = response.status;
    
    if (response.status === 'connected') {
       this.currentStatus = "connected";

      // Logged into your app and Facebook.
      // this.loginToSite();
      console.info("already login into app and facebook");

    }else{
       FB.login(function(response){
       	   if (response.status != "connected") {
       	   	   alert("login error!");
       	   }else{
       	   	   alert("login Successfully!");
       	   }
       });
    } 
 }

}

	
setTimeout(function(){
	// facebookApi.init();
	// fbAsyncInit();
  	// facebookApi.checkLoginState();
}, 1000);
