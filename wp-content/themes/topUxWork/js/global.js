/**
* all the global functions are here
**/
jQ = jQuery.noConflict();

function suggestWork()
{
	var suggestWorkModal = jQ("#suggestWork"),
	    form = suggestWorkModal.find('.ui.form'),
		formValidationRules ={
				url: {
				  identifier : 'portfolio',
				  rules: [
					{
					  type   : 'url',
					  prompt : 'Please enter a valid url'
					}
				  ]
				},
		},
        formSettings ={
			onSuccess : function() 
			{
			  suggestWorkModal.modal('hide');
			},
		};

	form.form(formValidationRules, formSettings);

	//modal settings
	//Note that if settings is incorrect, the modal may freeze, and won't output any console error or such
	suggestWorkModal.modal({
		detachable: true, 
		//By default, if click outside of modal, modal will close
		//Set closable to false to prevent this
		closable: false, 
		// transition: 'fade up',
		//Callback function for the submit button, which has the class of "ok"
		onApprove : function() {
		  //Submits the semantic ui form
		  //And pass the handling responsibilities to the form handlers, e.g. on form validation success
		  form.submit();
		  //Return false as to not close modal dialog
		  return false;
		},
	});
	
	jQ("#Submit").on("click", function(){
		//Resets form input fields
		form.trigger("reset");
		//Resets form error messages
		suggestWorkModal.find('.ui.form .field.error').removeClass( "error" );
		suggestWorkModal.find('.ui.form.error').removeClass( "error" );
		suggestWorkModal.modal('show');
	});
};


function initCards(){
	jQ('.special.cards .image').dimmer({
	  on: 'hover'
	});
};


function uiInit(){
	initCards();
	
	jQ('.ui.accordion').accordion();
}

jQ(document).ready(function() {
	suggestWork();
	uiInit();
});