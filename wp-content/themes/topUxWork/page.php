 <?php
    get_header();
 ?> 
    <body>
		<?php
			get_userInfo();
		?>
		<div class="ui container">
			<?php
 				get_menus();
			?>

 			<?php
              $post = get_post();
            ?>			
           	<h1 class="ui center aligned header">
           		<?php
           			// var_dump($post);
           			echo ucfirst($post->post_title);
           		?>
		     <div class="sub header"></div>
			</h1>
			<div class="ui hidden divider"></div>
			<div class="ui hidden divider"></div>
			<div class="ui hidden divider"></div>
			<div class="ui hidden divider"></div>
			<div class="ui text container">
				<?php
              		echo  apply_filters("the_content",$post->post_content);
				?>			
			</div>

			<?php
			  get_footer();
			?>

		</div>



    </body>
</html>
