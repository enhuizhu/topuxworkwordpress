<?php
add_theme_support( 'post-thumbnails' );

global $errorMsg, $userInfo;

if (isset($_POST["postLogin"])) {
    $result = custom_login($_POST["username"], $_POST["password"]);
    if (!$result["success"]) {
        $errorMsg = "username or password is wrong";
    }
}

$login = isUserLoggedIn();

if ($login) {
	$userInfo = getUserInfo($login["platform"]);
}


if (!function_exists("get_userInfo")) {
	function get_userInfo(){
	    return false;
	    global $userInfo;
	    /**
	    * should check if user already post form 
	    **/
	    if (isset($_POST["postLogin"]) && !empty($_POST["postLogin"])) {
	    	$result = custom_login($_POST["username"], $_POST["password"]);
	    	if (!$result["success"]) {
	    		$error = "username or password is not correct!";
	    	}
	    }


	    $content = '<div class="content-container">';
	    $content .= '<div class="ui container">';

	    $result = isUserLoggedIn();
	    
	    if ($result) {
			$userInfo = getUserInfo($result["platform"]);
			$content .= "<div class='content'>";
			$content .= "welcome " .$userInfo["username"];
    		$content .= '<a href ="'.getLogoutUrl($result["platform"]).'">Logout</a>';
			$content .= "</div>";	    	

	    }else{
	    	$content .= '<div class="content">';
	    	if (isset($error) && !empty($error)) {
	    		$content.='<span class="error">'.$error.'</span>';
	    	}
	    	$content .= '<a href="javascript:void(0)" class="login" id="loginLink">Login</a>';
	    	$content .= '<a href="'.getFacebookLoginUrl().'" class="fackebookLogin" id="loginWithFacebook">
	    				<i class="facebook square icon"></i>
	    				Login With Facebook</a>';
	    	$content .= '</div>';
	    }

	    $content .= '<div class="clear"></div>';

	    $content .= "</div>";
	    $content .= "</div>";

	    echo $content;
	}
}

/**
* all the theme functions are here
**/
if (!function_exists("get_menus")) {
   
   function get_menus(){
	     $items = wp_get_nav_menu_items("main"); 

	   	 $content ='<div class="overlay"></div>';

       $content .= '<div class="ui hidden divider"></div>
	   	 		<div class="ui secondary  menu ">
	    		<div class="menu" id="responsiveMenu">';
		 
		 foreach ($items as $item) {
		 		 $extraClass = get_the_title() == $item->title ? " active" : "";
		 		 $content = $content . '<a class="item'.$extraClass.'" href="'.$item->url.'">'.$item->title.'</a>';
		 }		
		 
		 $content.='</div>
	    		<div class="right menu">';
     $content.='<a href="javascript:void(0)" class="launch icon item"><i class="content icon"></i></a>';
		 
		 $result = isUserLoggedIn();

		 if ($result) {
			 $content.='<a href="'.getLogoutUrl($result["platform"]).'" class="item"><i class="user icon"></i> Logout</a>';
		 }else{
			 $content.='<a class="item" id="loginLink">Login</a>';
		 }
		 
		 $content.=	'<div class="item">
						<div class="ui primary button" id="Submit">Suggest a work</div>
					</div>
				</div>
			</div>
		';

	    echo $content;
 }
}

add_action("wp_ajax_loadPage", "loadPage");
add_action("wp_ajax_nopriv_loadPage", "loadPage");
function loadPage(){
	// die(var_dump($_POST));
	getCollections($_POST["currentPage"], true);
	die();
}


function getCollections($currentPage = 1, $isAjax = false){
    global $dbService, $userInfo;
    $postPerPage = 9;
    $offset = $postPerPage * ($currentPage - 1);

    $args = array(
        'post_type' =>  array( 'collection' ),
        'orderby' => 'date',
        'order' => 'desc',
        'posts_per_page' => $postPerPage,
        'offset' => $offset,
        'paged' =>$currentPage
    );

    $collections = new WP_Query( $args );
    if ($collections->have_posts()) {
        while ($collections->have_posts()) {
        $collections->the_post();
        $postId = get_the_id();
        $link = get_permalink($postId);
        $featureImg = wp_get_attachment_url( get_post_thumbnail_id($postId) );
        $firstName = esc_html( get_post_meta( $postId, 'firstName', true ));
        $lastName = esc_html( get_post_meta( $postId, 'lastName', true ));
        $description = esc_html( get_post_meta( $postId, 'description', true));
        $fullName = ucfirst($firstName)." ".ucfirst($lastName);
        $avatar = get_post_meta( $postId, "avatar", true);
        $url = esc_html(get_post_meta($postId, 'url', true));
        $jobTitle = esc_html(get_post_meta($postId, "jobTitle", true));
        $totalLike = $dbService->getTotalLike($postId);
        
        $isUserAlreadyLikeThisPost = $dbService->isUserAlreadyLikePost($postId, $userInfo["id"], $userInfo["platform"]);
?>

<div class="fluid card<?php echo $isAjax?" fadein-animation":""?>">
    <a class="dimmable image" href="<?php echo $link?>">
      <div class="ui dimmer">
        <div class="content">
          <div class="center">
            <i class="huge zoom icon"></i>
          </div>
        </div>
      </div>
      <img src="<?php echo $featureImg?>">
    </a>
    <div class="content">
        <a href="<?php echo $url?>"><img class="ui avatar image right floated" src="<?php echo $avatar["url"];?>"></a>
        <div class="header"><?php echo $fullName?></div>
        <div class="meta">
          <span class="date"><?php echo $jobTitle?></span>
        </div>
        <div class="description">
            <?php echo $description;?>
        </div>
    </div>
    <div class="extra content like<?php echo $isUserAlreadyLikeThisPost ? " disable" : "" ?>" data-postId="<?php echo $postId?>">
        <a>
          <?php
              $heartClass = $isUserAlreadyLikeThisPost ? "" : " outline";
          ?>
          <i class="heart like icon<?php echo $heartClass;?>"></i>
          <span class="like-number"><?php echo $totalLike;?></span> likes
        </a>
    </div>
    <div class="ui dimmer liked-layer">
        <div class="content">
          <div class="center">
            <h2 class="ui inverted icon header">
              <i class="heart red icon"></i>
              Liked!
            </h2>
          </div>
        </div>
    </div>
</div>
                
<?php
    }
   }else{
   	  echo "false";
   }

  }
?>


