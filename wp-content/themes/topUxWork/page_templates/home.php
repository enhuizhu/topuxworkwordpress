<?php
/**
 * Template Name: home
 *
 * @package WordPress
 */
?>

 <?php
    get_header();
 ?> 
    <body>
		<div class="ui container">
			<?php
 				get_menus();
			?>
			<h1 class="ui center aligned header">Top UX Work
				<div class="sub header">Collection of Top UX Portfolios</div>
			</h1>
			<div class="ui hidden divider"></div>
			<div class="ui hidden divider"></div>
			<div class="ui hidden divider"></div>
			<div class="ui hidden divider"></div>
			<div class="ui three doubling stackable special cards link">
				<div class="fluid card">
					<a class=" dimmable image" href="item-1.html">
				      <div class="ui dimmer">
				        <div class="content">
				          <div class="center">
				            <i class="huge zoom icon"></i>
				          </div>
				        </div>
				      </div>
				      <img src="<?php echo get_bloginfo("template_url")."/img/"; ?>/tumblr_ns93rn3y0X1ubgdseo1_500.png">
				    </a>
					<div class="content">
						<img class="ui avatar image right floated" src="<?php echo get_bloginfo("template_url")."/img/"; ?>/profile-John.png">
						<a class="header" href="#">John Ellison</a>
						<div class="meta">
						  <span class="date">Storyteller & UX Designer</span>
						</div>
						<div class="description">
						  John Ellison is from John Ellison LLC.
						</div>
					</div>
					<div class="extra content">
						<a>
						  <i class="heart outline like icon"></i>
					      17 likes
						</a>
					</div>
				</div>


				<div class="fluid card">
					<a class=" dimmable image" href="http://www.google.com">
				      <div class="ui dimmer">
				        <div class="content">
				          <div class="center">
				            <i class="huge zoom icon"></i>
				          </div>
				        </div>
				      </div>
				      <img src="<?php echo get_bloginfo("template_url")."/img/"; ?>/tumblr_nrz15tGkdP1ubgdseo1_r1_500.png">
				    </a>
				  <div class="content">
					<img class="ui avatar image right floated" src="<?php echo get_bloginfo("template_url")."/img/"; ?>/profile-kerem.jpg">
				    <a class="header">Kerem Suer</a>
				    <div class="meta">
				      <span class="date">Product Designer at Operator</span>
				    </div>
				    <div class="description">
				    </div>
				  </div>
				  <div class="extra content">
				    <a>
					  <i class="heart outline like icon"></i>
				      22 likes
				    </a>
				  </div>
				</div>

				<div class="fluid card">
				  <a class=" dimmable image" href="http://www.google.com">
				      <div class="ui dimmer">
				        <div class="content">
				          <div class="center">
				            <i class="huge zoom icon"></i>
				          </div>
				        </div>
				      </div>
				      <img src="<?php echo get_bloginfo("template_url")."/img/"; ?>/tumblr_ns6caoYJ2j1ubgdseo1_r2_500.png">
				    </a>
				  <div class="content">
					<img class="ui avatar image right floated" src="<?php echo get_bloginfo("template_url")."/img/"; ?>/profile-Adham.jpg">
				    <a class="header">Adham Dannaway</a>
				    <div class="meta">
				      <span class="date">UX/UI Designer</span>
				    </div>
				    <div class="description">
				      Adham Dannaway is from Sydney, Australia.
				    </div>
				  </div>
				  <div class="extra content">
				    <a>
					  <i class="heart outline like icon"></i>
				      22 likes
				    </a>
				  </div>
				</div>

				<div class="fluid card">
					<a class=" dimmable image" href="http://www.google.com">
				      <div class="ui dimmer">
				        <div class="content">
				          <div class="center">
				            <i class="huge zoom icon"></i>
				          </div>
				        </div>
				      </div>
				      <img src="<?php echo get_bloginfo("template_url")."/img/"; ?>/tumblr_ns6fr9tRw61ubgdseo1_r1_500.png">
				    </a>
					<div class="content">
						<img class="ui avatar image right floated" src="<?php echo get_bloginfo("template_url")."/img/"; ?>/profile-alex.jpg">
						<a class="header ">Alex Fox</a>
						<div class="meta">
						  <span class="date">Senior Designer at Quantcast
						  </span>
						</div>
						<div class="description">
							<!-- Alex've been a designer for over 5+ years now. He spends alot of my free time in coffee shops, bobbing to Spotify, sketching layouts and mashing keys as if Sublime Text were Street Fighter. -->
						</div>
					</div>
					<div class="extra content">
						<a>
						  <i class="heart outline like icon"></i>
					      17 likes
						</a>
					</div>
				</div>

				<div class="fluid card">
					<a class=" dimmable image" href="http://www.google.com">
				      <div class="ui dimmer">
				        <div class="content">
				          <div class="center">
				            <i class="huge zoom icon"></i>
				          </div>
				        </div>
				      </div>
				      <img src="<?php echo get_bloginfo("template_url")."/img/"; ?>/tumblr_ns6fjsySto1ubgdseo1_1280.png">
				    </a>
					<div class="content">
						<img class="ui avatar image right floated" src="<?php echo get_bloginfo("template_url")."/img/"; ?>/profile-Ryan.gif">
						<a class="header ">Ryan Scherf</a>
						<div class="meta">
						  <span class="date">Minneapolis based designer
						  </span>
						</div>
						<div class="description">
						  Designer building Paid and Payment to manage Stripe Connect accounts. Experimenting with Symbolist and Folllowing, writing open source utilities like Switcher and GitHubLink. 
						</div>
					</div>
					<div class="extra content">
						<a>
						  <i class="heart outline like icon"></i>
					      17 likes
						</a>
					</div>
				</div>

				<div class="fluid card">
					<a class=" dimmable image" href="http://www.google.com">
				      <div class="ui dimmer">
				        <div class="content">
				          <div class="center">
				            <i class="huge zoom icon"></i>
				          </div>
				        </div>
				      </div>
				      <img src="<?php echo get_bloginfo("template_url")."/img/"; ?>/tumblr_ns6f4srmPQ1ubgdseo1_r1_1280.png">
				    </a>
					<div class="content">
						<img class="ui avatar image right floated" src="<?php echo get_bloginfo("template_url")."/img/"; ?>/profile-Geoff.jpg">
						<a class="header ">Geoff Kimball</a>
						<div class="meta">
						  <span class="date">web + mobile designer
						  </span>
						</div>
						<div class="description">
						  Geoff lives in San Jose, California and works at the agency ZURB as a product designer.
						</div>
					</div>
					<div class="extra content">
						<a>
						  <i class="heart outline like icon"></i>
					      17 likes
						</a>
					</div>
				</div>

				<div class="fluid card">
					<a class=" dimmable image" href="http://www.google.com">
				      <div class="ui dimmer">
				        <div class="content">
				          <div class="center">
				            <i class="huge zoom icon"></i>
				          </div>
				        </div>
				      </div>
				      <img src="<?php echo get_bloginfo("template_url")."/img/"; ?>/tumblr_ns6fx9U0Uu1ubgdseo1_500.png">
				    </a>
					<div class="content">
						<img class="ui avatar image right floated" src="<?php echo get_bloginfo("template_url")."/img/"; ?>/profile-Kevin.jpg">
						<a class="header ">Kevin Schaefer</a>
						<div class="meta">
						  <span class="date">product Designer at Facebook
						  </span>
						</div>
						<div class="description">
						</div>
					</div>
					<div class="extra content">
						<a>
						  <i class="heart outline like icon"></i>
					      17 likes
						</a>
					</div>
				</div>

				<div class="fluid card ">
					<a class=" dimmable image" href="http://www.google.com">
				      <div class="ui dimmer">
				        <div class="content">
				          <div class="center">
				            <i class="huge zoom icon"></i>
				          </div>
				        </div>
				      </div>
				      <img src="<?php echo get_bloginfo("template_url")."/img/"; ?>/tumblr_ns6fyuFdrW1ubgdseo1_500.png">
				    </a>
					<div class="content">
						<img class="ui avatar image right floated" src="<?php echo get_bloginfo("template_url")."/img/"; ?>/profile-Mat.jpeg">
						<a class="header ">Mat Helme</a>
						<div class="meta">
						  <span class="date">Designer @Google 
						  </span>
						</div>
						<div class="description">
						  an Art Director & Visual Designer currently designing at Google
						</div>
					</div>
					<div class="extra content">
						<a>
						  <i class="heart outline like icon"></i>
					      17 likes
						</a>
					</div>
					<div class="ui dimmer ">
					    <div class="content">
					      <div class="center">
					        <h2 class="ui inverted icon header">
					          <i class="heart icon"></i>
					          Liked!
					        </h2>
					      </div>
					    </div>
					</div>
				</div>

				<div class="fluid card">
					<a class=" dimmable image" href="http://www.google.com">
				      <div class="ui dimmer">
				        <div class="content">
				          <div class="center">
				            <i class="huge zoom icon"></i>
				          </div>
				        </div>
				      </div>
				      <img src="<?php echo get_bloginfo("template_url")."/img/"; ?>/tumblr_ns6g6oYeWV1ubgdseo1_500.png">
				    </a>
					<div class="content">
						<img class="ui avatar image right floated" src="<?php echo get_bloginfo("template_url")."/img/"; ?>/profile-Stef.jpeg">
						<a class="header ">Stef Ivanov</a>
						<div class="meta">
						  <span class="date">freelance UX UI Designer from London   
						  </span>
						</div>
						<div class="description">
						  
						</div>
					</div>
					<div class="extra content">
						<a>
						  <i class="heart outline like icon"></i>
					      17 likes
						</a>
					</div>
				</div>

				<div class="fluid card">
					<a class=" dimmable image" href="http://www.google.com">
				      <div class="ui dimmer">
				        <div class="content">
				          <div class="center">
				            <i class="huge zoom icon"></i>
				          </div>
				        </div>
				      </div>
				      <img src="<?php echo get_bloginfo("template_url")."/img/"; ?>/tumblr_ns93me8WBP1ubgdseo1_500.png">
				    </a>
					<div class="content">
						<img class="ui avatar image right floated" src="<?php echo get_bloginfo("template_url")."/img/"; ?>/profile-Jackie.jpg">
						<a class="header ">Jackie Ngo</a>
						<div class="meta">
						  <span class="date">Web Platform Design Lead at Uber
						  </span>
						</div>
						<div class="description">
						</div>
					</div>
					<div class="extra content">
						<a>
						  <i class="heart outline like icon"></i>
					      17 likes
						</a>
					</div>
				</div>

				<div class="fluid card">
					<a class=" dimmable image" href="http://www.google.com">
				      <div class="ui dimmer">
				        <div class="content">
				          <div class="center">
				            <i class="huge zoom icon"></i>
				          </div>
				        </div>
				      </div>
				      <img src="<?php echo get_bloginfo("template_url")."/img/"; ?>/tumblr_ns6g03FtfU1ubgdseo1_500.png">
				    </a>
					<div class="content">
						<img class="ui avatar image right floated" src="<?php echo get_bloginfo("template_url")."/img/"; ?>/profile-simonpan.png">
						<a class="header ">Simon Pan</a>
						<div class="meta">
						  <span class="date">Senior User Experience Designer at Amazon Music
						  </span>
						</div>
						<div class="description">
						</div>
					</div>
					<div class="extra content">
						<a>
						  <i class="heart outline like icon"></i>
					      17 likes
						</a>
					</div>
				</div>
				<div class="fluid card">
					<a class=" dimmable image" href="http://www.google.com">
				      <div class="ui dimmer">
				        <div class="content">
				          <div class="center">
				            <i class="huge zoom icon"></i>
				          </div>
				        </div>
				      </div>
				      <img src="<?php echo get_bloginfo("template_url")."/img/"; ?>/tumblr_nsaxuwuTgj1ubgdseo1_500.png">
				    </a>
					<div class="content">
						<img class="ui avatar image right floated" src="<?php echo get_bloginfo("template_url")."/img/"; ?>/profile-michael.jpg">
						<a class="header ">Michael Szczepanski</a>
						<div class="meta">
						  <span class="date">Senior User Experience Designer at The Nerdery
						  </span>
						</div>
						<div class="description">
						  
						</div>
					</div>
					<div class="extra content">
						<a>
						  <i class="heart outline like icon"></i>
					      17 likes
						</a>
					</div>
				</div>

				<div class="fluid card">
					<a class=" dimmable image" href="http://www.google.com">
				      <div class="ui dimmer">
				        <div class="content">
				          <div class="center">
				            <i class="huge zoom icon"></i>
				          </div>
				        </div>
				      </div>
				      <img src="<?php echo get_bloginfo("template_url")."/img/"; ?>/tumblr_nvlh9ejL4F1ubgdseo1_500.png">
				    </a>
					<div class="content">
						<img class="ui avatar image right floated" src="<?php echo get_bloginfo("template_url")."/img/"; ?>/profile-Shane.jpg">
						<a class="header ">Shane Dudfield</a>
						<div class="meta">
						  <span class="date">UX Designer based in NYC
						  </span>
						</div>
						<div class="description">
						</div>
					</div>
					<div class="extra content">
						<a>
						  <i class="heart outline like icon"></i>
					      17 likes
						</a>
					</div>
				</div>

			</div>

			<?php
			  get_footer();
			?>

		</div>



    </body>
</html>
